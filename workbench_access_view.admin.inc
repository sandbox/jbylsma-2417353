<?php

/**
 * @file workbench_access_view.admin.inc
 * Administration pages and forms for workbench_access_view permissions.
 *
 * TODO: Big todo, fix text for revoke permissions
 */

/**
 * Settings form.
 */
function workbench_access_view_settings($form, &$form_state) {
  $form = array();

  $form['help'] = array(
    '#markup' => '<h2>' . t('Workbench Access View settings') . '</h2>',
  );

  $form['workbench_access_view_access_method'] = array(
    '#title' => t('Access method'),
    '#description' => t('Workbench Access typically uses "Grant" access.'),
    '#type' => 'radios',
    '#options' => array(
      'grant' => t('Grant'),
      'revoke' => t('Revoke'),
    ),
    '#default_value' => variable_get('workbench_access_view_access_method', 'grant'),
  );

  $form['workbench_access_view_inherit_permissions'] = array(
    '#title' => t('Inherited permission configuration'),
    '#type' => 'radios',
    '#options' => array(
      'no' => t('Set view permissions independent of editorial permissions'),
      'some' => t('Allow editors to view content in assigned sections'),
      'yes' => t('Inherit all view settings from editorial permissions'),
    ),
    '#default_value' => variable_get('workbench_access_view_inherit_permissions', 'some'),
    '#states' => array(
      'visible' => array(
        ':input[name="workbench_access_view_access_method"]' => array('value' => 'grant'),
      ),
    ),
  );

  return system_settings_form($form);
}

/**
 * @param string $scope
 *   "user" or "role".
 */
function workbench_access_view_configure($scope, $access_type = NULL, $access_id = NULL) {
  $access_info = FALSE;
  if ($access_type !== NULL && $access_id !== NULL) {
    $access_info = workbench_access_view_access_info_load($access_type, $access_id);
  }

  if ($access_info) {
    $function = "workbench_access_view_configure_{$scope}";
    return $function($access_type, $access_info);
  }
  else {
    $function = "workbench_access_view_list_{$scope}";
    return $function();
  }
}

/**
 * Load a workbench_access item.
 */
function workbench_access_view_access_info_load($access_type, $access_id) {
  module_load_include('inc', 'workbench_access', 'modules/taxonomy.workbench_access');

  $access_info = workbench_access_load_access_info(array(
    'access_type' => $access_type,
    'access_id' => $access_id,
  ));

  // If the access item failed to load, the access_id will probably be empty.
  if (empty($access_info['access_id'])) {
    return FALSE;
  }

  return $access_info;
}

/**
 * List workbench access sections with a count of users who may view content in
 * each section.
 */
function workbench_access_view_list_user() {
  $active = workbench_access_get_active_tree();
  $roles = workbench_access_get_roles('access workbench_access_view by role');

  // Get a count of users with view permissions per section.
  $counts = array();
  if (!empty($roles)) {
    $query = db_select('workbench_access_view_user', 'workbench_access_view_user')
      ->condition('workbench_access_view_user.access_scheme', $active['access_scheme']['access_scheme'])
      ->fields('workbench_access_view_user', array('access_id'));

    // If the "Authenticated User" role does not have the "Allow all members of
    // this role to be assigned to Workbench Access sections" permission, then we
    // need to only count uids with the appropriate role.
    if (!isset($roles[DRUPAL_AUTHENTICATED_RID])) {
      $users_roles_alias = $query->join('users_roles', 'users_roles', 'workbench_access_view_user.uid = %alias.uid');
      $query
        ->condition("$users_roles_alias.rid", array_keys($roles))
        ->groupBy("$users_roles_alias.uid");
    }

    $query->groupBy('workbench_access_view_user.access_id');
    $count_alias = $query->addExpression('COUNT(workbench_access_view_user.uid)');
    $counts = $query->execute()->fetchAllKeyed(0, 1);
  }

  $render = array(
    '#theme' => 'workbench_access_view_admin',
    '#title' => t('View permissions by user'),
    '#header' => array(t('Section'), t('Viewers')),
    '#rows' => _workbench_access_view_admin_table_rows($active, $counts, 'admin/config/workbench/access/editors/view', '1 viewer', '@count viewers'),
  );

  _workbench_access_view_admin_list_add_intro($render, $roles);
  return $render;
}

function workbench_access_view_list_role() {
  $active = workbench_access_get_active_tree();
  $roles = workbench_access_get_roles('access workbench_access_view by role');

  // Get a count of roles with view permissions per section.
  $counts = array();
  if (!empty($roles)) {
    $query = db_select('workbench_access_view_role', 'workbench_access_view_role')
      ->condition('workbench_access_view_role.access_scheme', $active['access_scheme']['access_scheme'])
      ->condition('workbench_access_view_role.rid', array_keys($roles))
      ->fields('workbench_access_view_role', array('access_id'))
      ->groupBy('workbench_access_view_role.access_id');
    $count_alias = $query->addExpression('COUNT(*)');
    $counts = $query->execute()->fetchAllKeyed(0, 1);
  }

  $render = array(
    '#theme' => 'workbench_access_view_admin',
    '#title' => t('View permissions by role'),
    '#header' => array(t('Section'), t('Roles')),
    '#rows' => _workbench_access_view_admin_table_rows($active, $counts, 'admin/config/workbench/access/roles/view', '1 role', '@count roles'),
  );

  _workbench_access_view_admin_list_add_intro($render, $roles);
  return $render;
}

/**
 * Build rows for tables on the workbench_access_view permissions configuration
 * pages.
 */
function _workbench_access_view_admin_table_rows($active, $counts, $base_url, $singular, $plural) {
  $rows = array();

  foreach ($active['tree'] as $access_id => $section) {
    if (!isset($active['active'][$access_id])) {
      continue;
    }

    $count = (isset($counts[$access_id]) ? $counts[$access_id] : 0);
    $url = "{$base_url}/{$active['access_scheme']['access_type']}/{$access_id}";
    $row = array(
      str_repeat('-', $section['depth'] ) . ' ' . l($section['name'], $url),
      l(format_plural($count, $singular, $plural), $url),
    );

    $rows[] = $row;
  }

  return $rows;
}

function _workbench_access_view_admin_list_add_intro(&$render, $roles) {
  if (empty($roles)) {
    unset($render['#body'], $render['#header'], $render['#rows']);
    if (user_access('administer permissions')) {
      $render['#intro'] = t('To continue, at least one role must be have the !permission permission.', array(
        '!permission' => l('Allow all members of this role to be assigned to view Workbench Access sections', 'admin/people/permissions', array('fragment' => 'module-workbench_access_view'))
      ));
    }
    else {
      $render['#intro'] = t('There are no roles who have permission to edit sections. Please contact a site administrator.');
    }
  }
  else {
    $render['#intro'] = t('The following sections are currently active.');
    if (user_access('administer workbench access')) {
      $render['#intro'] .= ' ' . t('You may !enable_or_disable.', array('!enable_or_disable' => l(t('enable or disable sections'), 'admin/config/workbench/access/sections')));
    }
  }
}

/**
 * Configure users' viewing permissions for a workbench_access section.
 */
function workbench_access_view_configure_user($access_type, $access_info) {
  $active = workbench_access_get_active_tree();

  // Deny access if this is not the active access type or if this item isn't in
  // the current user's active items.
  if ($active['access_scheme']['access_type'] != $access_type || empty($active['active'][$access_info['access_id']])) {
    drupal_access_denied();
    drupal_exit();
  }

  // Get a list of accounts with access to this section.
  $query = db_select('users', 'u')
    ->fields('u', array('uid', 'name'));

  $alias = $query->join('workbench_access_view_user', 'workbench_access_view_user', 'u.uid = %alias.uid');
  $query
    ->condition("$alias.access_scheme", $active['access_scheme']['access_scheme'])
    ->condition("$alias.access_id", $access_info['access_id'])
    ->extend('PagerDefault')
    ->limit(25);

  // If the "Authenticated User" role does not have the "Allow all members of
  // this role to be assigned to Workbench Access sections" permission, then we
  // need to limit this list to uids with the appropriate role.
  $roles = workbench_access_get_roles('access workbench_access_view by role');
  if (!isset($roles[DRUPAL_AUTHENTICATED_RID])) {
    $users_roles_alias = $query->join('users_roles', 'users_roles', 'workbench_access_view_user.uid = %alias.uid');
    $query
      ->condition("$users_roles_alias.rid", array_keys($roles))
      ->groupBy("$users_roles_alias.uid");
  }

  $accounts = $query->execute()->fetchAll();

  // Add information to the access item info, since this is required when saving
  // permissions.
  $access_info += array(
    'access_scheme' => $active['access_scheme']['access_scheme'],
    'access_type' => $access_type,
  );

  // Build the form that manipulates this list.
  $form = drupal_get_form('workbench_access_view_configure_user_form', $access_info, $accounts);

  // Build the rest of the page.
  $render = array(
    '#theme' => 'workbench_access_view_admin',
    '#title' => t('%name viewers by account', array('%name' => $access_info['name'])),
    '#intro' => t('Active viewers for the %section section, as assigned by account.', array('%section' => $access_info['name'])) . ' ' . l(t('View viewers by role'), "admin/config/workbench/access/roles/view/{$access_info['access_type']}/{$access_info['access_id']}") . '.',
    '#body' => drupal_render($form),
  );

  return $render;
}

function workbench_access_view_configure_user_form($form, &$form_state, $access_info, $accounts) {
  $form = array();

  $form['workbench_access_item'] = array(
    '#type' => 'value',
    '#value' => $access_info,
  );

  $form['users'] = array(
    '#tree' => TRUE,
    '#theme' => 'workbench_access_view_admin_form_table',
    '#header' => array(t('Account'), t('Remove')),
  );

  foreach ($accounts as $account) {
    $form['users'][$account->uid]['label'] = array(
      '#markup' => l($account->name, 'user/' . $account->uid),
    );
    $form['users'][$account->uid]['remove'] = array(
      '#title' => t('Remove'),
      '#type' => 'checkbox',
    );
  }

  if (empty($accounts)) {
    $form['users'][] = array(
      'label' => array('#markup' => t('No active users have been found.')),
      'remove' => array('#markup' => '')
    );
  }

  $form['add_user'] = array(
    '#type' => 'textfield',
    '#title' => t('Add viewer'),
    '#autocomplete_path' => "workbench_access_view/autocomplete/{$access_info['access_scheme']}/{$access_info['access_id']}",
    '#element_validate' => array('_workbench_access_view_add_user_element_validate'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update viewers'),
  );

  return $form;
}

/**
 * Validate the add_user field.
 */
function _workbench_access_view_add_user_element_validate($element, &$form_state) {
  if (!empty($element['#value'])) {
    $account = user_load_by_name($element['#value']);
    if (empty($account)) {
      form_error($element, t('The selected user does not exist.'));
    }
    elseif (!user_access('access workbench_access_view by role', $account)) {
      form_error($element, t('The selected user does not have permission to be added as a viewer.'));
    }
  }
}

function workbench_access_view_configure_user_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $access_info = $form_state['values']['workbench_access_item'];

  // If there's a value in the add_user field, add this section to the user's
  // workbench access sections.
  if (!empty($values['add_user'])) {
    $account = user_load_by_name($values['add_user']);
    if (empty($account->workbench_access[$access_info['access_id']]['view']) || !in_array('all', $account->workbench_access[$access_info['access_id']]['view'])) {
      $account->workbench_access[$access_info['access_id']]['view'] = array('all');
      workbench_access_view_user_sections_save($account, $access_info['access_scheme']);
    }
  }

  // If the 'remove' checkbox was checked for any users, remove this section
  // from their workbench access sections.
  if (!empty($values['users'])) {
    foreach ($values['users'] as $uid => $actions) {
      if ($actions['remove']) {
        $account = user_load($uid);
        $account->workbench_access[$access_info['access_id']]['view'] = array();
        workbench_access_view_user_sections_save($account, $access_info['access_scheme']);
      }
    }
  }
}

/**
 * Configure roles' viewing permissions for a workbench_access section.
 */
function workbench_access_view_configure_role($access_type, $access_info) {
  $active = workbench_access_get_active_tree();

  // Deny access if this is not the active access type or if this item isn't in
  // the current user's active items.
  if ($active['access_scheme']['access_type'] != $access_type || empty($active['active'][$access_info['access_id']])) {
    drupal_access_denied();
    drupal_exit();
  }

  // Get a list of roles with access to this section.
  $all_roles = workbench_access_get_roles('access workbench_access_view by role');
  $enabled_roles = db_select('workbench_access_view_role', 'workbench_access_view_role')
    ->fields('workbench_access_view_role', array('rid'))
    ->condition('workbench_access_view_role.rid', array_keys($all_roles))
    ->condition('access_scheme', $active['access_scheme']['access_scheme'])
    ->condition('access_id', $access_info['access_id'])
    ->execute()
    ->fetchCol();

  // Get an array of user accounts that have access to this section.
  $accounts = array();
  if (!empty($enabled_roles)) {
    // Get a list of uids that have the enabled roles.
    $query = db_select('users', 'users')
      ->fields('users', array('uid'))
      ->groupBy('users.uid');

    if (!empty($enabled_roles) && !in_array(DRUPAL_AUTHENTICATED_RID, $enabled_roles)) {
      $users_roles_alias = $query->join('users_roles', 'users_roles', 'users.uid = %alias.uid');
      $query->condition("$users_roles_alias.rid", $enabled_roles);
    }

    if (!in_array(DRUPAL_ANONYMOUS_RID, $enabled_roles)) {
      $query->condition('users.uid', 0, '!=');
    }

    $uids = $query
      ->extend('PagerDefault')
      ->limit(25)
      ->execute()
      ->fetchCol();

    // Load the user accounts.
    $accounts = user_load_multiple($uids);
  }

  // Add information to the access item info, since this is required when saving
  // permissions.
  $access_info += array(
    'access_scheme' => $active['access_scheme']['access_scheme'],
    'access_type' => $access_type,
  );

  // Build the form that manipulates this list.
  $form = drupal_get_form('workbench_access_view_configure_role_form', $access_info, $all_roles, $enabled_roles, $accounts);

  // Build the rest of the page.
  $render = array(
    '#theme' => 'workbench_access_view_admin',
    '#title' => t('%name viewers by role', array('%name' => $access_info['name'])),
    '#intro' => t('Active viewers for the %section section, as determined by role.', array('%section' => $access_info['name'])) . ' ' . l(t('View viewers by account'), "admin/config/workbench/access/editors/view/{$access_info['access_type']}/{$access_info['access_id']}") . '.',
    '#body' => drupal_render($form),
  );

  return $render;
}

function workbench_access_view_configure_role_form($form, &$form_state, $access_info, $all_roles, $enabled_roles, $accounts) {
  $form = array();

  $form['workbench_access_item'] = array(
    '#type' => 'value',
    '#value' => $access_info,
  );

  $form['users'] = array(
    '#tree' => TRUE,
    '#theme' => 'workbench_access_view_admin_form_table',
    '#header' => array(t('Account'), t('Roles')),
  );

  foreach ($accounts as $account) {
    $form['users'][$account->uid]['label'] = array(
      '#markup' => l($account->name, 'user/' . $account->uid),
    );
    $form['users'][$account->uid]['roles'] = array(
      '#theme' => 'item_list',
      '#items' => $account->roles,
    );
    unset($form['users'][$account->uid]['roles']['#items'][DRUPAL_AUTHENTICATED_RID]);
  }

  if (empty($accounts)) {
    $form['users'][] = array(
      'label' => array('#markup' => t('No active roles have been found.')),
      'remove' => array('#markup' => '')
    );
  }

  $form['roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Roles'),
    '#options' => $all_roles,
    '#default_value' => $enabled_roles,
    '#description' => t('Select the roles that should have all users assigned to this section.')
  );

  if (isset($all_roles[DRUPAL_AUTHENTICATED_RID])) {
    $form['roles']['#description'] .= ' ' . t('Selecting the authenticated user role will select all registered users.');
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update viewers'),
  );

  return $form;
}

function workbench_access_view_configure_role_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $rids = array_filter($form_state['values']['roles']);
  workbench_access_view_role_sections_save($rids, $values['workbench_access_item']['access_id'], $values['workbench_access_item']['access_scheme']);
}

/**
 * Autocomplete callback for the user view permissions configuration form.
 * @see workbench_access_view_configure_user_form()
 */
function workbench_access_view_autocomplete($access_type, $access_id, $string) {
  $active = workbench_access_get_active_tree();
  $roles = workbench_access_get_roles('access workbench_access_view by role');
  $query = db_select('users', 'users')
    ->addTag('user_autocomplete')
    ->fields('users', array('name'))
    ->condition('users.name', db_like($string) . '%', 'LIKE')
    ->groupBy('users.uid')
    ->range(0, 10);

  // Filter out users who already have access to this group.
  $current_users_alias = $query->leftJoin('workbench_access_view_user', 'workbench_access_view_user', 'users.uid = %alias.uid AND %alias.access_scheme = :access_scheme AND %alias.access_id = :access_id', array(
    ':access_scheme' => $active['access_scheme']['access_scheme'],
    ':access_id' => $access_id,
  ));
  $query->isNull("$current_users_alias.uid");

  // If the "Authenticated User" role does not have permission to be assigned
  // view permissions on workbench_access sections, limit the list by role.
  if (!isset($roles[DRUPAL_AUTHENTICATED_RID])) {
    $users_roles_alias = $query->join('users_roles', 'users_roles', 'users.uid = %alias.uid');
    $query->condition("$users_roles_alias.rid", $roles, 'NOT IN');
  }

  $matches = $query->execute()->fetchAllKeyed(0, 0);
  drupal_json_output($matches);
}
