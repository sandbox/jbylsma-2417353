In order for any user role, including anonymous and authenticated users, to see
any content controlled by workbench_access with this module enabled, the user
role needs to:

* Have the workbench_access "Allow all members of this role to be assigned to
  view Workbench Access sections" permission:
  admin/people/permissions#module-workbench_access_view

* Be assigned to a workbench_access viewer role at:
  Configuration > Workbench > Workbench Access > Roles > Viewers
  admin/config/workbench/access/roles/view/taxonomy/workbench_access

On installation, Workbench Access View grants the anonymous and authenticated
roles both the permission and the viewer role to the top-level Workbench Access
section, so that previously visible content is not hidden without explicit
configuration by an administrator.
